#!/usr/bin/env python
import setuptools

packages = setuptools.find_packages(
    exclude=['*.tests', '*.tests.*', 'tests.*', 'tests'])

setuptools.setup(
    name='Telegram-Notifier',
    description='The service bots for telegram',
    author='Anton Antonov',
    packages=packages,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.8',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    entry_points={
        'console_scripts': [
            '%s=%s:main' % (name.replace('_', '-'), name)
            for name in packages[:1]]
    },
    test_suite='tests',
)