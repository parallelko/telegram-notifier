from aiogram import Bot
from aiohttp import ClientSession
from motor.motor_asyncio import AsyncIOMotorClient
from .io.mongo_proxy import PydanticMongoProxy

from . import Settings
from .base.app import BaseService
from .notifier import TelegramNotifier
from .dispatcher import TelegramDispatcher
from .scheduler import APScheduler


class TelegramNotifierService(BaseService):
    def __init__(self, settings: Settings) -> None:
        super().__init__()
        http_session = ClientSession()
        self.add_stop_callback(http_session.close)
        self.bot = Bot(settings.telegram_token)
        self.notifier = TelegramNotifier(
            self.bot,
            settings.telegram_channel_id,
        )
        self.add_stop_callback(self.bot.close)

        mongo_motor = AsyncIOMotorClient(settings.mongo_uri)
        self.mongo_client = PydanticMongoProxy(mongo_motor)
        self.add_start_callback(mongo_motor.get_server_info)
        self.add_start_callback(mongo_motor.disconnect)

        self.scheduler = APScheduler()
        self.add_start_callback(self.scheduler.start)
        self.add_stop_callback(self.scheduler.stop)

        self.dp = TelegramDispatcher(
            settings,
            self.bot,
            self.scheduler,
            self.notifier,
            self.mongo_client,
            http_session,
        )
        self.add_stop_callback(self.dp.stop)

    async def main(self):
        await self.dp.serve()
