from .base.app import AppRunner
from .base.logging import setup_logging
from .settings import Settings
from .service import TelegramNotifierService


def main():
    setup_logging()
    settings = Settings()
    runner = AppRunner(
        service=TelegramNotifierService(settings),
        app_name='telegram-notifier',
        settings=settings
    )
    runner.start()
