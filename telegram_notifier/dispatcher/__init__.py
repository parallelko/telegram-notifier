import asyncio
import logging
from abc import ABC, abstractmethod
from aiogram import Bot, Dispatcher as AiogramDispatcher
from functools import partial
from aiohttp import ClientSession
from telegram_notifier import Settings
from telegram_notifier.notifier import Notifier
from telegram_notifier.scheduler import Scheduler
from ..io.mongo_proxy import PydanticMongoProxy
from ..jobs import (
    search_flat
)


class Dispatcher(ABC):
    @abstractmethod
    async def start(self):
        ...

    @abstractmethod
    async def stop(self):
        ...

    @abstractmethod
    async def serve(self):
        ...


class TelegramDispatcher(Dispatcher):
    def __init__(
            self,
            settings: Settings,
            bot: Bot,
            scheduler: Scheduler,
            notifier: Notifier,
            mongo_client: PydanticMongoProxy,
            http_session: ClientSession,
    ) -> None:
        self.settings = settings
        self.bot = bot
        self.dp = AiogramDispatcher(bot)
        self.scheduler = scheduler
        self.notifier = notifier
        self.mongo_client = mongo_client
        self.http_session = http_session
        self.register_handlers()

    def register_handlers(self):
        self.dp.register_message_handler(
            self.start_command_handler,
            cmd=['start']
        )

    async def start_command_handler(self, *args, **kwargs):
        pass

    async def start(self):
        asyncio.create_task(self.dp.start_polling())

    async def stop(self):
        self.dp.stop_polling()

    async def serve(self):
        await self.dp.start_polling()
