from pydantic import BaseModel
from typing import Optional


class FlatSearchResult(BaseModel):
    name: str
    link: str
    source: str
    price: int
    currency: Optional[str]
    address: Optional[str]


class FlatSearchFilters(BaseModel):
    price: Optional[int]
