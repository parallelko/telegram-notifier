import logging


def setup_logging(level: str = None):
    cfg = {
        'format': '[%(levelname)1.1s %(asctime)s.%(msecs)03d '
                  '%(process)d] %(message)s',
        'datefmt': '%Y-%m-%d %H:%M:%S',
        'level': getattr(
            logging, level.upper() if level else "",
            logging.DEBUG
        )
    }
    logging.basicConfig(**cfg, force=True)
    return cfg
