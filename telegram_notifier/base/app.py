import abc
import asyncio
import logging
import signal
import sys
from enum import IntEnum, Enum, auto
from typing import MutableSet, Callable, Optional, List

from pydantic import BaseSettings

from telegram_notifier.base.logging import setup_logging


class ExitCode(IntEnum):
    Success = 0
    Error = 1


class RunnerStatus(Enum):
    Starting = auto()
    Running = auto()
    Stopping = auto()


class BaseService(abc.ABC):
    def __init__(self) -> None:
        self._start_callbacks: MutableSet[Callable] = set()
        self._stop_callbacks: MutableSet[Callable] = set()

    def add_start_callback(self, func):
        if callable(func):
            self._start_callbacks.add(func)

    def add_stop_callback(self, func):
        if callable(func):
            self._stop_callbacks.add(func)

    async def start(self):
        for cb in self._start_callbacks:
            await cb()

    async def stop(self):
        for cb in self._stop_callbacks:
            try:
                result = cb()
                if asyncio.iscoroutine(result):
                    await result
            except Exception as error:
                logging.exception(
                    f'Error happened during calling service stop_callback: '
                    f'{error}'
                )

    @abc.abstractmethod
    async def main(self):
        ...


class RunnerSettings(BaseSettings):
    # Seconds to wait for main task stop for graceful shutdown
    wait_for_main_stop: int = 3
    # Seconds to wait for service shutdown callbacks finished
    wait_for_service_stop: int = 3


class AppRunner:
    def __init__(
            self,
            service: BaseService,
            app_name: Optional[str] = None,
            settings: Optional[BaseSettings] = None,
            runner_settings: Optional[RunnerSettings] = None,
            loop: Optional[asyncio.AbstractEventLoop] = None,
            default_metrics_tags: Optional[List[str]] = None
    ):
        self._service: BaseService = service
        self._settings: Optional[BaseSettings] = settings
        self._runner_settings: RunnerSettings = \
            runner_settings or RunnerSettings()
        self._app_name = app_name or self.__class__.__name__
        self._default_metrics_tags = default_metrics_tags
        self._loop: asyncio.AbstractEventLoop = \
            loop or asyncio.get_event_loop()
        self._main_task: Optional[asyncio.Future] = None
        self._health_check_task: Optional[asyncio.Future] = None
        self._stop_future: asyncio.Future = asyncio.Future()
        self._status: RunnerStatus = RunnerStatus.Starting

    def log_settings(self):
        if self._settings:
            show = '\n'.join(
                [f'{k} = {v}' for k, v in self._settings.dict().items()])
            logging.info(f'App {self._app_name} settings:\n'
                         f'{show}')

    def start(self):
        self._status = RunnerStatus.Starting
        if sys.platform != 'win32':
            def signal_handler(s):
                asyncio.create_task(self.stop(s, ExitCode.Success))

            signals = (signal.SIGHUP, signal.SIGTERM, signal.SIGINT)
            for s in signals:
                self._loop.add_signal_handler(
                    s, lambda s=s: signal_handler(s))

        try:
            self.log_settings()
            self._loop.run_until_complete(self._service.start())
            self._main_task = self._loop.create_task(
                self._run_main())
            self._main_task.add_done_callback(self._on_main_stop)
            self._status = RunnerStatus.Running
        except BaseException as error:
            logging.exception(
                f'Service startup {self._app_name} failed: {error}'
            )
            exit_code = ExitCode.Error
        else:
            self._loop.run_until_complete(self._stop_future)
            exit_code = self._stop_future.result()
        finally:
            self._graceful_shutdown()
        sys.exit(exit_code)

    async def stop(self, sig: int, exit_code: ExitCode):
        if self._status == RunnerStatus.Stopping:
            return
        self._status = RunnerStatus.Stopping
        if self._health_check_task:
            self._health_check_task.cancel()
        try:
            await asyncio.wait_for(
                self._service.stop(),
                timeout=self._runner_settings.wait_for_service_stop
            )
        except BaseException as error:
            logging.error(f'Error happened during service stop: {error}')

        if self._main_task and not self._main_task.done():
            self._main_task.cancel()

            try:
                await asyncio.wait_for(
                    self._main_task,
                    timeout=self._runner_settings.wait_for_main_stop
                )
            except asyncio.CancelledError:
                pass
            except BaseException as error:
                logging.error(f'Error happened during main stop: {error}')
            # Find all running tasks:
        pending = asyncio.all_tasks()
        # Run loop until tasks done:

        current_task = asyncio.current_task()
        sig_name = signal.strsignal(sig)
        logging.info(f"Received exit signal {sig_name}...")
        tasks = [t for t in pending if t is not current_task]

        if tasks:
            for task in tasks:
                # skipping over shielded coro still does not help
                if task.get_coro().__name__ == "cant_stop_me":
                    continue
                logging.info(f'Tasks to cancel: {task}')
                task.cancel()

            logging.info(f"Cancelling outstanding tasks ({len(tasks)})")
            await asyncio.gather(*tasks, return_exceptions=True)
        # await asyncio.gather(*pending)
        # event_loop.run_until_complete(asyncio.gather(*pending))
        # self._loop.stop()
        await asyncio.sleep(0)
        self._stop_future.set_result(exit_code)
        logging.info("Shutdown complete ...")

    async def _run_main(self):
        try:
            await self._service.main()
        except asyncio.CancelledError:
            await self.stop(signal.SIGTERM, ExitCode.Success)
        except BaseException as error:
            logging.exception(f'Service failed with error: {error}')
            raise

    def _on_main_stop(self, task: asyncio.Task):
        if not self._status == RunnerStatus.Stopping:
            exit_code = ExitCode.Success
            try:
                error = task.exception()
                if error:
                    exit_code = ExitCode.Error
            except asyncio.CancelledError:
                pass
            loop = asyncio.get_event_loop()
            loop.create_task(self.stop(signal.SIGTERM, exit_code))

    def _graceful_shutdown(self, *a, **k):
        self._loop.close()
