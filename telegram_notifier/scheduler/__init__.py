import asyncio
from typing import Callable, Awaitable, Coroutine, Optional

from apscheduler.schedulers.asyncio import AsyncIOScheduler
from abc import ABC, abstractmethod


class Scheduler(ABC):
    @abstractmethod
    async def start(self) -> None:
        ...

    @abstractmethod
    async def stop(self) -> None:
        ...

    @abstractmethod
    def start_job(self, task: Callable[[], Awaitable]) -> None:
        ...

    @abstractmethod
    def add_cron_job(
            self, task: Callable[[], Awaitable], cron_tab: str,
            name: Optional[str] = None,
    ) -> None:
        ...


class APScheduler(Scheduler):
    def __init__(self) -> None:
        self.scheduler = AsyncIOScheduler()

    async def start(self) -> None:
        self.scheduler.start()

    async def stop(self) -> None:
        self.scheduler.shutdown()

    def start_job(self, task: Callable[[], Awaitable]) -> None:
        self.scheduler.add_job(task)

    def add_cron_job(
            self,
            task: Callable[[], Awaitable],
            cron_tab: str,
            name: Optional[str] = None,
    ) -> None:
        cron = cron_tab.split(' ')
        if len(cron) != 5:
            raise ValueError('Wrong cron tab')
        self.scheduler.add_job(
            task, 'cron',
            minute=cron[0],
            hour=cron[1],
            day=cron[2],
            month=cron[3],
            day_of_week=cron[4],
            name=name,
        )
