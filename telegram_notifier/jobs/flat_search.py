import asyncio
import logging
from abc import ABC, abstractmethod
from typing import List, Sequence, Optional
from aiohttp import ClientSession
from bs4 import BeautifulSoup
from telegram_notifier.base.models import FlatSearchResult, FlatSearchFilters
from telegram_notifier.io.mongo_proxy import PydanticMongoProxy
from telegram_notifier.notifier import Notifier
import re

page_re = re.compile("page(\d+)")
tr_finder = re.compile('tr_\d+')
price_finder = re.compile("([\d,]+)\s")


def found_pages(base_soup: BeautifulSoup) -> int:
    pages = base_soup.find('a', class_='navi', rel='prev')
    if pages:
        url = pages.attrs.get('href', "")
        page_search = page_re.search(url)
        if page_search:
            return int(page_search.group(1))
    else:
        return 0


def find_page_results(form_soup: BeautifulSoup, base_url: str) -> List[
    FlatSearchResult]:
    start_table = form_soup.find_all('tr', id=tr_finder)
    ret = []
    element: BeautifulSoup
    for element in start_table:
        link = element.find('a', class_='am')
        price = price_finder.search(
            element.contents[9].text
        )
        if not price:
            continue
        addr = element.contents[3].text
        result = FlatSearchResult(
            name=link.text,
            link=base_url + link.attrs['href'],
            source='SS.com',
            price=int(price.group(1).replace(',', '')),
            address=addr,
        )
        ret.append(result)
    return ret


class FlatSource(ABC):
    @abstractmethod
    async def search_flats(
            self, filters: FlatSearchFilters
    ) -> List[FlatSearchResult]:
        ...


class SSComSource(FlatSource):
    def __init__(
            self,
            session: ClientSession,
            notifier: Notifier,
            url='https://www.ss.com/ru/real-estate/flats/riga/today-2/'
    ) -> None:
        self.url = url
        self.base_url = url.split('.com')[0] + '.com'
        self.session = session
        self.notifier = notifier

    async def search_flats(
            self, filters: FlatSearchFilters,
    ) -> List[FlatSearchResult]:
        try:
            async with self.session.get(self.url) as resp:
                text = await resp.text()
                soup = BeautifulSoup(text, 'html.parser')
                pages = found_pages(soup)

                form: Optional[BeautifulSoup] = soup.find(
                    'form', id='filter_frm'
                )
                if not form:
                    await self.notifier.send_message('SS com form not found')
                    logging.warning('Not found form with Adds')
                page_results = find_page_results(form, self.base_url)
        except ValueError:
            await self.notifier.send_message('Parsing failed')
            raise
        if filters.price:
            page_results = [result for result in page_results if
                            result.price <= filters.price]
        return page_results


async def search_flat(
        notifier: Notifier,
        session: ClientSession,
        db: PydanticMongoProxy,
):
    filters = FlatSearchFilters(price=400)
    sources: Sequence[FlatSource] = [
        SSComSource(session, notifier)
    ]
    sources_results: Sequence[List[FlatSearchResult]] = await asyncio.gather(
        *[s.search_flats(filters) for s in sources]
    )
    # TODO: Add check for new

    await asyncio.gather(
        *[notifier.send_flat_result(result) for result in sources_results]
    )
