from pydantic import BaseSettings


class Settings(BaseSettings):
    mongo_uri: str = ''
    telegram_token: str = ''
    telegram_channel_id: int
