import datetime
import typing as t
from decimal import Decimal
from functools import singledispatch

A = t.TypeVar('A')

Encoder = t.Callable[[A], t.Any]


# Value encoder instances
@singledispatch
def mongo_encoder(a: A, encoder: Encoder = None) -> A:
    return a


@mongo_encoder.register
def decimal_encoder(x: Decimal, encoder=mongo_encoder) -> str:
    return str(x)


@mongo_encoder.register
def date_encoder(x: datetime.date, encoder=mongo_encoder):
    return datetime.datetime.combine(x, datetime.datetime.min.time())


@mongo_encoder.register(list)
@mongo_encoder.register(tuple)
@mongo_encoder.register(set)
def list_encoder(x, encoder=mongo_encoder) -> list:
    return [encoder(a) for a in x]


@mongo_encoder.register
def dict_encoder(x: dict, encoder=mongo_encoder) -> dict:
    return {k: encoder(v) for k, v in x.items()}
