import copy
from typing import List, Dict, Optional, Tuple, Type, TypeVar
from motor.motor_asyncio import AsyncIOMotorClient
from pydantic import BaseModel
from .encoder import mongo_encoder

T = TypeVar('T', bound=BaseModel)


class PydanticMongoProxy:
    def __init__(self, client: AsyncIOMotorClient):
        self._client = client
        self._db = self._client.get_database()

    async def get(
            self,
            model: Type[T],
            query: Dict,
    ) -> Optional[T]:
        collection = self._get_collection(model)
        data = await collection.find_one(query)

        if data:
            return self._to_model(model, data)

        return None

    async def find_iter(
            self,
            model: Type[T],
            query: Optional[Dict] = None,
            sort: List[Tuple[str, int]] = None,
            skip: Optional[int] = None,
            limit: Optional[int] = None,
            timeout=None,
    ):
        collection = self._get_collection(model)
        cursor = collection.find(
            query,
            max_time_ms=sec_to_ms(timeout),
        )
        if sort is not None:
            cursor = cursor.sort(sort)

        if skip is not None:
            cursor = cursor.skip(skip)

        if limit is not None:
            cursor = cursor.limit(limit)

        async for record in cursor:
            item = self._to_model(model, record)
            yield item

    async def find(
            self,
            model: Type[T],
            query: Optional[Dict] = None,
            sort: List[Tuple[str, int]] = None,
            skip: Optional[int] = None,
            limit: Optional[int] = None,
            timeout=None,
    ) -> List[T]:
        result = []
        async for record in self.find_iter(
                model, query, sort, skip, limit, timeout=timeout
        ):
            result.append(record)
        return result

    async def count(
            self,
            model: Type[T],
            query: Optional[Dict] = None
    ) -> int:
        collection = self._get_collection(model)

        if not query:
            count = await collection.estimated_document_count()
        else:
            count = await collection.count_documents(query)

        return count

    async def create(
            self,
            model: T,
    ):
        collection = self._get_collection(model)
        data = mongo_encoder(self._to_document(model))
        await collection.insert_one(data)

    async def upsert(
            self,
            model: T,
            query: Dict,
    ):
        collection = self._get_collection(model)
        data = mongo_encoder(self._to_document(model))
        await collection.replace_one(query, data, upsert=True)

    async def delete(
            self,
            model: Type[T],
            query: Dict,
    ):
        collection = self._get_collection(model)
        await collection.delete_one(query)

    async def bulk_write(
            self, model: Type[T],
            operations: List,
            ordered=True,
    ):
        collection = self._get_collection(model)

        return await collection.bulk_write(
            self._update_operations(model, operations),
            ordered=ordered,
        )

    @classmethod
    def _update_operations(cls, model, operations):
        result = []

        for operation in operations:
            operation = copy.copy(operation)

            for field_name in ('_doc', '_filter'):
                field = getattr(operation, field_name, None)

                if isinstance(field, model):
                    value = cls._to_document(field)
                    setattr(operation, field_name, value)

            result.append(operation)

        return result

    def _get_collection(self, model: T | Type[T]):
        if isinstance(model, BaseModel):
            cls = model.__class__
        elif issubclass(model, BaseModel):
            cls = model
        else:
            raise ValueError('Wrong argument type: model')
        collection_name = cls.__name__
        collection = self._db[collection_name]
        return collection

    @classmethod
    def _to_model(cls, model: Type[T], data: Dict):
        return model(**data)

    @classmethod
    def _to_document(cls, model: T):
        return model.dict(exclude_none=True, by_alias=True)


def sec_to_ms(seconds: Optional[int] = None):
    if seconds:
        return seconds * 1000
    return None
