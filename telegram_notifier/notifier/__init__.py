import asyncio
from abc import ABC, abstractmethod
from typing import List

from aiogram import Bot

from telegram_notifier.base.models import FlatSearchResult


class Notifier(ABC):
    @abstractmethod
    async def send_flat_result(
            self,
            search_result: List[FlatSearchResult]
    ) -> None:
        ...

    @abstractmethod
    async def send_message(self, text: str) -> None:
        ...


class TelegramNotifier(Notifier):
    def __init__(self, bot: Bot, channel_id: int) -> None:
        self.bot = bot
        self.channel_id = channel_id

    async def send_flat_result(
            self,
            search_result: List[FlatSearchResult]
    ) -> None:
        for result in search_result:
            text = (f'{result.name}'
                    f'\nprice: {result.price}'
                    f'\nlink: {result.link}'
                    f'\naddress: {result.address}'
                    )
            await self.bot.send_message(self.channel_id, text)
            await asyncio.sleep(2)

    async def send_message(self, text: str) -> None:
        await self.bot.send_message(self.channel_id, text)
